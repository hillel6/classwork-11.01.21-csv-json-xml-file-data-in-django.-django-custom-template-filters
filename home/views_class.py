import csv

from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse, FileResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.base import View

from home.forms import StudentForm
from home.models import Student


class StudentView(View):

    def get(self, request):
        """
        Show all students in template
        """
        # выбрать всех студентов из таблицы students(джанго сама
        # дописывает `s` в окончании таблицы, но модели всегда должны
        # быть в единственном числе)
        students = Student.objects.\
            select_related('book', 'subject').\
            prefetch_related('teachers').all()

        # для рендеринга темплейта нужно использовать render
        # эта функция принимает минимум два параметра request, template_name
        return render(
            request=request,
            template_name='student_list.html',
            context={
                'students': students,
                'string': 'Some string with spaces',
            }
        )


class StudentAddView(View):

    def get(self, request):
        """
        Create student by student's name
        """
        # все GET параметры хранятся в request.GET
        student_name_from_request = request.GET.get('name')

        if not student_name_from_request:
            return HttpResponse('Student name missing')

        # Чтобы создать нового юзера нужно инициализировать Модель
        student = Student()
        # далее сохраняем параметры в объект модели
        student.name = student_name_from_request
        # сохранение
        student.save()

        return HttpResponse('Student {} have been created'.format(student.name))


class StudentCreateView(View):

    def get(self, request):
        """
        Create student by Django Forms
        """
        # генерируем форму
        student_form = StudentForm()

        # добавляем форму в контекст
        context = {
            'student_form': student_form,
        }

        return render(request, 'student_form.html', context=context)

    def post(self, request):
        # получаем данные с формы
        student_form = StudentForm(request.POST)
        # сохраняем данные в таблицу
        # если все валидно
        if student_form.is_valid():
            student_form.save()

        return redirect(reverse('class_students_list'))


class StudentUpdateView(View):

    def get(self, request, id):
        """
        Create student by Django Forms
        """
        student = get_object_or_404(Student, id=id)

        # генерируем форму
        student_form = StudentForm(instance=student)

        # добавляем форму в контекст
        context = {
            'student_form': student_form,
            'student': student,
        }

        return render(request, 'student_update.html', context=context)

    def post(self, request, id):
        student = get_object_or_404(Student, id=id)

        # получаем данные с формы
        student_form = StudentForm(request.POST, instance=student)
        # сохраняем данные в таблицу
        # если все валидно
        if student_form.is_valid():
            student_form.save()

        return redirect(reverse('class_students_list'))


class JsonView(View):

    def get(self, request):
        # взятие списка студентов
        students = Student.objects.all()
        # взятие одного студента
        student = Student.objects.last()

        return JsonResponse({
            "message": "Hello World!",
            "student": model_to_dict(student),  # через model_to_dict можно конвертировать инстанс модели в словарь
            "students": list(students.values(  # через values можно конвертировать инстансы модели в словари
                "name",
                "book__title",
                "subject__title",
            )),
        })


class CSVView(View):

    def get(self, request):
        response = HttpResponse(content_type="text/csv")

        response['Content-Disposition'] = "attachment; filename=data_students.csv"

        # Создаем обертку на респонсом для записи данных
        writer_for_response = csv.writer(response)
        # Первая строка это строка Головы (информации об столбиках)
        writer_for_response.writerow(["Name", "Book", "Subject"])

        students = Student.objects.all()
        for student in students:
            # Остальные столбики записываются как тело CSV файла
            writer_for_response.writerow([
                student.name,
                student.book.title if student.book else None,
                student.subject.title if student.subject else None,
            ])

        return response


class FileView(View):

    def get(self, request):

        with open("pyproject.toml") as file:
            response = FileResponse(file.read())
            # Для выгрузки файла нужно указывать Content-Disposition с первым
            # параметром attachment и вторым названием файла
            response['Content-Disposition'] = "attachment; filename={}".format(file.name)

            return response


class XMLView(View):

    def get(self, request):
        # отображение XML файла происходит также как и обычных файлов, только можно дописывать тип файл
        with open("test_xml.xml") as file:
            response = FileResponse(file.read())

            response['Content-Type'] = 'text/xml'

            return response
