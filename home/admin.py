from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin

from home.models import Student

# Имя класса может быть любое но в основном указывают Имя модели + Admin
class StudentAdmin(ModelAdmin):
    # Список полей который нужно отображать на таблице-списке модели
    list_display = ('pk', 'name', 'custom_field')
    # Список полей по которым можно в админке фильтровать таблицу
    list_filter = ('name',)
    # Список полей по которым будет происходить поиск в админке
    search_fields = ('pk', 'name',)
    # Список полей по которым будет происходить сортировка в админке для данной таблицы
    ordering = ('-name',)
    # Поля которые должны быть только чтение на странице редактирования записи
    readonly_fields = ('name',)

    def custom_field(self, instance):
        """
        Кастомное поле, то поле которого нет в модели но нужно отобразить на таблице-списке
        данных текущей модели,
        Данный метод принимает объект той модели которой мы привизали в admin.site.register
        """
        return "Custom field of {}".format(instance.name)


# Класс админка нужно ассоциировать с моделью
admin.site.register(Student, StudentAdmin)
