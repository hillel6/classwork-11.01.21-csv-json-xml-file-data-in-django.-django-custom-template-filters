import uuid

from django.core.management.base import BaseCommand
from faker import Faker

from home.models import Student, Book, Teacher, Subject


# имя файла является название команды

class Command(BaseCommand):

    # описание команды располагается в параметре help
    help = 'Add new student(s) to the system'

    # все аргументы парсяться в этом методе
    def add_arguments(self, parser):

        parser.add_argument('-l', '--len', type=int, default=2)

    # сама логика команды распологается здесь
    def handle(self, *args, **options):

        # для работы с Faker надо его заинициализировать
        faker = Faker()

        # для вывода информации о выполнении команды лучше использовать self.stdout.write вместо print
        self.stdout.write('Start inserting Students')
        for _ in range(options['len']):
            self.stdout.write('Start inserting Students')
            book = Book()
            book.title = uuid.uuid4()
            book.save()

            subject, _ = Subject.objects.get_or_create(title='Python')

            student = Student()
            # faker имеет большое количество разной генерируемой информации один из них name()
            student.name = faker.name()
            student.book = book
            student.subject = subject
            student.save()

            teacher, _ = Teacher.objects.get_or_create(name=faker.name())
            teacher.students.add(student)
            teacher.save()

        self.stdout.write('End inserting Students')
