from celery import chain, chord, group, shared_task


@shared_task
def sum_number(*args):
    return sum(args)


@shared_task
def chain_numbers():
    # выполнение всех тасков последовательно
    return chain(sum_number.s(1, 2), sum_number.s(3, 4))()


@shared_task
def group_numbers():
    # выполнение всех тасков паралельно
    return group([sum_number.s(1, 2), sum_number.s(3, 4)])()


@shared_task
def chord_numbers():
    # выполнение всех тасков паралельно, но после того как все выполнится выполнить sum_task
    return chord([sum_number.s(1, 2), sum_number.s(3, 4)])(sum_task.si())


@shared_task
def sum_task(*args):
    return args
